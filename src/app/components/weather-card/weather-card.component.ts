import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent implements OnInit {
  @Input() weatherData: object;
  constructor() { }

  ngOnInit() {
  }

  getDateObj (value: number) {
    return new Date(value * 1000);
  }
}
