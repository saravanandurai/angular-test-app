import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterCityComponent } from './enter-city.component';

describe('EnterCityComponent', () => {
  let component: EnterCityComponent;
  let fixture: ComponentFixture<EnterCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
