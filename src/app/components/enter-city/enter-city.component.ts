import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-enter-city',
  templateUrl: './enter-city.component.html',
  styleUrls: ['./enter-city.component.scss']
})
export class EnterCityComponent implements OnInit {

  city = '';
  errorFlag = false;
  errorMessage = '';
  constructor(
    private weatherService: WeatherService,
    private router: Router,
    private location: Location,
  ) { }

  ngOnInit() {
  }

  async showCityWeather() {
    if (!this.city) {
      this.errorMessage = 'City name cannot be blank';
      this.errorFlag = true;
    } else {
      const weatherData: any = await this.weatherService.getEnteredCityWeather(this.city);
      if (!weatherData) {
        this.errorMessage = 'City does not exist, check spelling';
        this.errorFlag = true;
      } else {
        this.router.navigate(['/city-weather'], {queryParams: {city: this.city}});
      }
    }
  }

  goBack(): void {
    this.location.back();
  }
}
