import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.scss']
})
export class CityWeatherComponent implements OnInit {

  errorFlag = false;
  domFlag = false;
  weatherData: any = null;

  constructor(
    private route: ActivatedRoute,
    private weatherService: WeatherService,
    private location: Location,
  ) { }

  async ngOnInit() {
    const data = await this.weatherService.getEnteredCityWeather(this.route.snapshot.queryParams.city);

    if (!data) {
      this.errorFlag = true;
    } else {
      this.weatherData = data;
      this.domFlag = true;
    }

  }

  goBack(): void {
    this.location.back();
  }

}
