import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { WeatherService } from '../../services/weather.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displayDom = false;
  currentLocationWeather: object;
  constructor(
    private weatherService: WeatherService,
    private router: Router,
  ) { }

  ngOnInit() {
    navigator.geolocation.getCurrentPosition((val) => {
      this.weatherService.getCurrentLocationWeather(val.coords.latitude, val.coords.longitude)
        .then(weatherData => {
            if (weatherData) {
              this.currentLocationWeather = weatherData;
              this.displayDom = true;
            }

        });
    }, (err) => {
      console.log('Something Broke!');
    });
  }

  showNextPage() {
    this.router.navigate(['/enter-city']);
  }

}
