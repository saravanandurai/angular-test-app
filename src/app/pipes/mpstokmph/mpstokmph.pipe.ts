import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mpstokmph'
})
export class MpstokmphPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Math.round((value * 60 * 60 / 1000) * 100) / 100;
  }

}
