import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempCentigrade'
})
export class TempCentigradePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Math.round((value - 273.15) * 100) / 100;
  }

}
