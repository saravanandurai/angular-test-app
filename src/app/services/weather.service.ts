import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  keys = '';
  url: string = 'http://api.openweathermap.org/data/2.5/weather?appid=' + this.keys;

  constructor() { }

  async getCurrentLocationWeather(lat: number, lon: number) {
    const response = await fetch(this.url + '&lat=' + lat + '&lon=' + lon);
    return response.status === 200 ? response.json() : null;
  }

  async getEnteredCityWeather(city: string) {
    const response = await fetch(this.url + '&q=' + city);
    return response.status === 200 ? response.json() : null;
  }
}
