import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './modules/app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';

import { MyMaterialModule } from './modules/my-material/my-material.module';
import { TempCentigradePipe } from './pipes/temp-centigrade/temp-centigrade.pipe';
import { MpstokmphPipe } from './pipes/mpstokmph/mpstokmph.pipe';
import { WeatherCardComponent } from './components/weather-card/weather-card.component';
import { EnterCityComponent } from './components/enter-city/enter-city.component';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    TempCentigradePipe,
    MpstokmphPipe,
    WeatherCardComponent,
    EnterCityComponent,
    CityWeatherComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
