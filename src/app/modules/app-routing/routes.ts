import { Routes } from '@angular/router';

import { HomeComponent } from '../../components/home/home.component';
import { EnterCityComponent } from '../../components/enter-city/enter-city.component';
import { CityWeatherComponent } from '../../components/city-weather/city-weather.component';

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'enter-city', component: EnterCityComponent },
    { path: 'city-weather', component: CityWeatherComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' }
];
